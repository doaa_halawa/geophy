
 	Feature: Seaarch Page
  Description: As a logged in user I want to view search page
    So that I can perform tasks related to it
    
  Background: Navigate to evra login page
   Given I navigate to evra login page
   
   
  Scenario: Perform valid search scenario
    Given I insert user credintials "validEmail" and "validPassword" and click login
    When I insert valid search criteria
	  Then Search results page should be displayed successfully
  
   
   Scenario Outline: Perform invalid search scenario
    When I insert search criteria "<address>" , "<income>" , "<numberOfUnits>" , "<YearOfConstruction>" and "<occupancy>"
	  Then Validation error is displayed below the field "<validatioError>"
	  And Run Valuation button is disabled

  Examples:
 	  |Scenario             |address           |income      |numberOfUnits           |YearOfConstruction           |occupancy           |validatioError         |
 	  |invalid address      |invalidAddress		 |validIncome |validNumberOfUnits      |validYearOfConstruction      |validOccupancy      | addressValidationError|
 	   