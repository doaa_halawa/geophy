package pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends PageBase
{
	public LoginPage(WebDriver driver) {
		super(driver);
	}
	@FindBy(id = "email")
	private WebElement email_Txtbox;
	
	@FindBy(id = "password")
	private WebElement password_Txtbox;

	@FindBy(css = "button[class='button button--primary']")
	private WebElement login_btn;
	
	@FindBy(xpath = "//span[@class='hidden lg:inline pr-2']")
	private WebElement greeting_Txt;
	
	public void login(String email , String password){
		email_Txtbox.sendKeys(email);	
		password_Txtbox.sendKeys(password);
		login_btn.click();
	}
	
	public String getGreetingText(){
		return this.getElementText(greeting_Txt);
	}
}
