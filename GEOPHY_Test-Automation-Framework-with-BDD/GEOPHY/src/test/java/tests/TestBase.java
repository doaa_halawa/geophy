package tests;


import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import cucumber.api.testng.AbstractTestNGCucumberTests;
import utilities.Helper;

public class TestBase extends AbstractTestNGCucumberTests  {

	public static WebDriver driver;

	@BeforeMethod(alwaysRun = true)
	@Parameters({ "browser" })
	protected void setUp(@Optional("chrome") String browser, ITestContext ctx) throws MalformedURLException {
		// Create Driver
		BrowserDriverFactory factory = new BrowserDriverFactory(browser);

		driver = factory.createDriver();

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
	}

	@AfterMethod(alwaysRun = true)
	protected void tearDown(ITestResult result) {
		
		// take screenshot when test case fail and add it in the Screenshot folder
		if (result.getStatus() == ITestResult.FAILURE)
		{
			System.out.println("Failed!");
			System.out.println("Taking Screenshot....");
			Helper.captureScreenshot(driver, result.getName());
		}
		// Closing driver
		driver.quit();
		
	}
	

}