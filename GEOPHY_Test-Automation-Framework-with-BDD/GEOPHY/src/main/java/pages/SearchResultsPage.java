package pages;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class SearchResultsPage extends PageBase
{
	public SearchResultsPage(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(css = ".border-black.border")
	private WebElement confidenceIndicator_icon;
	
	@FindBy(xpath = "//h4[@class='text-xl sm:text-3xl border-b-2 border-grey pt-3 pb-2']")
	private WebElement address_lbl;
	
	public String getAddress()
	{
		String address = getElementText(address_lbl);
		return address;
	}
	
	public String getConfidenceIndicatorColor()
	{
		String confidenceColor = getElementAttribute(confidenceIndicator_icon,"data-confidence-indicator");
		return confidenceColor;
	}

    public List<String> getSearchValues() {
    	List<String> valuesList = new ArrayList<String>();
    	valuesList = getListOfElemetsText(By.xpath("//table/tbody/tr/td[2]"));	
    	return valuesList;
    	}
    	
		}
 
    


