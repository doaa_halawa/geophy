package pages;
import java.awt.AWTException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchPage extends PageBase
{
	public SearchPage(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(id = "address_input")
	private WebElement address_Txt;
	
	@FindBy(id = "noi")
	private WebElement income_Txt;
	
	@FindBy(name = "number_of_units")
	private WebElement numberOfUnits_Txt;
	
	@FindBy(name = "year_built")
	private WebElement YearOfConstruction_Txt;
	
	@FindBy(name = "occupancy")
	private WebElement occupancy_Txt;
	 
	@FindBy(id = "introjsRunValuationButton")
	private WebElement runValuation_Button;
	
	@FindBy(xpath = "//div[@class='input-group pt-8 input-group--error']/span")
	private WebElement addressError_Label;

    
    public void evraSearch(String address, String income, String numberOfUnits, String YearOfConstruction,String occupancy) throws AWTException, InterruptedException {
    	address_Txt.sendKeys(address);
    	selectFirstItemInListUsingRobot();
    	income_Txt.sendKeys(income);
    	numberOfUnits_Txt.sendKeys(numberOfUnits);
    	YearOfConstruction_Txt.sendKeys(YearOfConstruction);
    	occupancy_Txt.sendKeys(occupancy);
    	click(runValuation_Button);
		}
    
    public String getAddresError()
    {
    	return getElementText(addressError_Label);
    }
    
    public String getRunValuationClass()
    {
    	return getElementAttribute(runValuation_Button, "class");
    } 

    }
