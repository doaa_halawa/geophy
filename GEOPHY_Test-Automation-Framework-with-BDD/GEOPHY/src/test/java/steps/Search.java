package steps;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import data.JsonDataReader;
import pages.*;
import tests.TestBase;

public class Search extends TestBase{
   SearchPage searchObject; 
   SearchResultsPage searchResultsObject;
   JsonDataReader json ;
   @When("^I insert search criteria \"([^\"]*)\" , \"([^\"]*)\" , \"([^\"]*)\" , \"([^\"]*)\" and \"([^\"]*)\"$")
   public void I_insert_search_criteria(String address ,String income,String numberOfUnits,String YearOfConstruction,String occupancy) throws Throwable {
	   json = new JsonDataReader();
	   searchObject = new SearchPage(driver);
	   searchObject.evraSearch(json.JsonReader(address), json.JsonReader(income), json.JsonReader(numberOfUnits), json.JsonReader(YearOfConstruction), json.JsonReader(occupancy));
   };
   
   @When("^I insert valid search criteria")
   public void I_insert_valid_search_criteria() throws Throwable {
	   json = new JsonDataReader();
	   searchObject = new SearchPage(driver);
	   searchObject.evraSearch(json.JsonReader("validAddress"), json.JsonReader("validIncome"), json.JsonReader("validNumberOfUnits"), json.JsonReader("validYearOfConstruction"), json.JsonReader("validOccupancy"));
   };
	
	@Then("^Search results page should be displayed successfully$")
	public void search_results_should_be_displayed_successfully() throws Throwable {
		json = new JsonDataReader();
		searchResultsObject = new SearchResultsPage(driver);
		List<String> tableList = new ArrayList<String>();
		tableList = searchResultsObject.getSearchValues();
		
		Assert.assertEquals(searchResultsObject.getAddress(),json.JsonReader("validAddress").toUpperCase());
		Assert.assertEquals(tableList.get(0),json.JsonReader("validNumberOfUnits"));
		Assert.assertEquals(tableList.get(1),json.JsonReader("validYearOfConstruction"));
		Assert.assertEquals(tableList.get(3),"$ "+json.JsonReader("validIncome"));
		Assert.assertEquals(tableList.get(5),json.JsonReader("validOccupancy")+"%");	
		
		Assert.assertEquals(searchResultsObject.getConfidenceIndicatorColor(),"green");	
	}
	
	@Then("^Validation error is displayed below the field \"([^\"]*)\"$")
	public void validation_error_is_displayed_below_the_field(String error) throws Throwable {
		json = new JsonDataReader();
		searchObject = new SearchPage(driver);	
		Assert.assertEquals(searchObject.getAddresError(),json.JsonReader(error));
	}
	
	@Then("^Run Valuation button is disabled$")
	public void runValuation_button_is_disabled() throws Throwable {
		json = new JsonDataReader();
		searchObject = new SearchPage(driver);	
		Assert.assertTrue(searchObject.getRunValuationClass().contains("button--disabled"));
	}


}
