package pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class PageBase {

	protected WebDriver driver ;  
	public Select select ; 
	WebDriverWait wait;
	
	public PageBase(WebDriver driver) 
	{
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	
	public void click(WebElement element) 
	{
		wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(element));
		element.click();
	}
	
	public String getElementText(WebElement element) {
		wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(element));
		String elementText = element.getText();
		return elementText;
	}
	
	public List<String> getListOfElemetsText(By selector) {
		wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
		List<String> textList = new ArrayList<String>();  
		List<WebElement> list = driver.findElements(selector);
		for (int i = 0; i < list.size(); i++)
		{
			textList.add(list.get(i).getText());
		}
		return textList;
	}
	
	public void  selectItemFromDDLUsingText(WebElement dropDownList , String text) {
		wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(dropDownList));
		select = new Select(dropDownList);
		select.selectByVisibleText(text);
	}
	
	public String getElementAttribute(WebElement element, String attribute) {
		String attributeValue = element.getAttribute(attribute);
		return attributeValue;
	}
	
	public void verifyURLContainsText(String Text)
	{
		Assert.assertTrue(driver.getCurrentUrl().contains(Text));
	}
	
	public void navigateToURL(String URL)
	{
		driver.navigate().to(URL);
	}
	
	public String getUniqueString() {
        return new SimpleDateFormat("yyyyMMdd.HHmmss.").format(new Date()) + String.valueOf(new Random().nextInt(100));
    }
	
	public void  selectFirstItemInListUsingRobot() throws AWTException {
		Robot robot = new Robot();
        robot.delay(1000);
        robot.keyPress(KeyEvent.VK_DOWN);
        robot.keyPress(KeyEvent.VK_ENTER);
	}
}
